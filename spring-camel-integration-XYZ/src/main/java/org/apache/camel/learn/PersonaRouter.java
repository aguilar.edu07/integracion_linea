package org.apache.camel.learn;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;


@Component
public class PersonaRouter extends RouteBuilder {
    
    JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Persona.class);

    public void configure() throws Exception {
        System.out.println("Hola ingrese al router");
        //from("direct:crearPersonaVacia")
        from("direct:crearPersonaEmpresa")
        .log("--------------------- Inicio Proceso --------------------------")
        .routeId("CrearPersona")
        .process(new CreatePersonProcesor())
        .marshal(jsonDataFormat)
        .process(exchange -> { 
            // Convertir el JSON a objeto Person
            String json = exchange.getIn().getBody(String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            Persona person = objectMapper.readValue(json, Persona.class);
            //Buscamos el perfil de compara para enviar a la empresa ABC o 123
            int perfilCompra = person.getPerfilCompra();

           // Agregar el ID como propiedad del intercambio
           exchange.setProperty("perfilCompra", person.getPerfilCompra());

            log.info("******* ",perfilCompra);
        })
        .log("Person object created: ${body}") // Logueamos el objeto Person
        .log("el perfilCompra enviado es:  ${exchangeProperty.perfilCompra} ") // Ver en cual perfil entro
        .choice() // Inicio de la construcción del Choice
            .when().simple("${exchangeProperty.perfilCompra} == 1")
                .log("El perfilCompra es 1")
                .to("rest:post:/Persona?host=localhost:5129")
            .when().simple("${exchangeProperty.perfilCompra} == 2")
                .log("El perfilCompra es 2")
                .to("rest:post:/persons?host=localhost:5000")
            .otherwise()
                    .log("El perfil de compra no es 1 ni 2")
        .end() // Fin del Choice
        .to("log:CREATE");
    }
}
