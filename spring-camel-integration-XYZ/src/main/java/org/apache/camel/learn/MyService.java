package org.apache.camel.learn;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyService {
    
    @Autowired private ProducerTemplate template;
    
    @GetMapping(value = "/createperson/")
    public String createPerson(){
        return template.requestBody("direct:crearPersonaVacia", "").toString();
    }

    //Metodo para enviar las personas
    @PostMapping(value = "/enviarPersona", consumes = "application/json")
    public String enviarPersona(@RequestBody Persona per){
        return template.requestBody("direct:crearPersonaEmpresa", per).toString();
    }

}
