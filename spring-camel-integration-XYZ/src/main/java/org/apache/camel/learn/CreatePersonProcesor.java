package org.apache.camel.learn;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CreatePersonProcesor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        System.out.println("Hola ingrese a crear la persona");
        Persona nueva = new Persona();

        String personaString = exchange.getIn().getBody(String.class);


        // Separar la cadena y obtener los valores de los atributos
        nueva.setCodigo(Integer.parseInt(personaString.split(",")[0].split("=")[1]));
        nueva.setIdentificacion(personaString.split(",")[1].split("=")[1]);
        nueva.setNombres(personaString.split(",")[2].split("=")[1]);
        nueva.setDireccion(personaString.split(",")[3].split("=")[1]);
        nueva.setPerfilCompra(Integer.parseInt(personaString.split(",")[4].split("=")[1].replace("]", "")));
/* 
        // Construir un objeto JSON con los valores de los atributos
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode personaJson = objectMapper.createObjectNode();
        personaJson.put("codigo", codigo);
        personaJson.put("identificacion", identificacion);
        personaJson.put("nombres", nombres);
        personaJson.put("direccion", direccion);
        personaJson.put("perfilCompra", perfilCompra);
*/
// Convertir el objeto JSON a una cadena
//String json = personaJson.toString();

// Imprimir el JSON resultante
//System.out.println(json);
/* 
        // Deserializar el JSON a una instancia de la clase Persona
        Persona persona = objectMapper.readValue(json, Persona.class);
*/
        // Establecer la instancia de Persona como el cuerpo del intercambio
        exchange.getIn().setBody(nueva);


        /* 
        nueva.setCodigo(9);
        nueva.setIdentificacion("0504030201");
        nueva.setDireccion("Quito");
        nueva.setNombres("Edu Aguilar R");
        nueva.setPerfilCompra(2);

        exchange.getIn().setBody(nueva);*/
    }
    
}
