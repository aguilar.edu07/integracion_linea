# integracion_linea
## Realizado por:
Eduardo Aguilar R.

# Como ejecutar

## 1 Proyecto Python
python main.py

http://localhost:5000/persons

--------------------------------------------------------------------------
## 2 Proyecto .NET
dotnet clean
dotnet build
dotnet run
http://localhost:5129/swagger/index.html

--------------------------------------------------------------------------
## 3 Proyecto Spring

mvn clean package -DskipTests

java -jar target\spring-camel-integration-xyz-1.0.0-SNAPSHOT.jar

ejecutar el archivo de postman adjunto llamado 
AgregarPersona.postman_collection
