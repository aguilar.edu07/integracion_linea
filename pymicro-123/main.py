import json
from flask import Flask, jsonify, request, abort

app = Flask(__name__)

data = [{
    "codigo":1,
    "identificacion":"0102030405",
    "nombres":"Eduardo Aguilar",
    "direccion": "Cuenca",
    "perfilCompra" : 2
},
{
    "codigo":2,
    "identificacion":"0101010101",
    "nombres":"Juan Perez",
    "direccion": "Quito",
    "perfilCompra" : 2
}]

@app.route('/persons', methods = ['GET'])
def returnpersonas():
    if (request.method == 'GET'):
        return jsonify(data)

@app.route('/persons/<int:codigo>', methods = ['GET'])
def returnpersona(codigo):
    if (request.method == 'GET'):
        for person in data:
            if (person.get("codigo")==codigo):
                return jsonify(person)
        abort(404)

@app.route('/persons', methods = ['POST'])
def addpersona():
    if (request.method == 'POST'):
        person = request.json
        data.append(person)
        return "OK"
    else:
        abort(440, "No Valido")    

if __name__ == '__main__':
    app.run(debug=True)