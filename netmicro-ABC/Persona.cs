public class Persona
{
    public Persona(int codigo, String identificacion, String nombres, String direccion, int perfilCompra ){
        this.codigo = codigo;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.direccion = direccion;
        this.perfilCompra = perfilCompra;
        
    }

    public int codigo {get;set;}

    public String nombres{get;set;}

    public String direccion{get;set;}

    public String identificacion{get;set;}

    public int perfilCompra {get;set;}
}